<?php

namespace Drupal\ckeditor_chart\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "chart" plugin.
 *
 * @CKEditorPlugin(
 *   id = "chart",
 *   label = @Translation("Chart")
 * )
 */
 
 class ckeditor_chart extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return 'libraries/ckeditor_chart/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Chart' => [
        'label' => t('Chart'),
        'image' => 'libraries/ckeditor_chart/icons/chart.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
